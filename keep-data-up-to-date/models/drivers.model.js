module.exports = (sequelize, DataTypes) => {
    const Drivers = sequelize.define("drivers", {
        accountCode: {
            type: DataTypes.STRING,
        },
        accName: {
            type: DataTypes.STRING,
        },
        lastName: {
            type: DataTypes.STRING
        },
        is_active: {
            type: DataTypes.INTEGER
        }
    });
    return Drivers;
};