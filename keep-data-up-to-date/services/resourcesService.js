module.exports = (function (db) {
    return {
        addBulkResources
    };

    function addBulkResources(data) {
        // db.Sequelize.Op
        return db.resources.bulkCreate(data)
    }

})
