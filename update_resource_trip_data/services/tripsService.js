const { Op } = require("sequelize");

module.exports = (function (db) {
    return {
        addTrip,
        getTripTimesForResourceId
    };

    function addTrip(data) {
        return db.trips.create(data)
    }

    function getTripTimesForResourceId(resourceId) {
        // console.log(`resourceId`, resourceId)
        return db.trips.findAll(
            {
                where: { resource_id: resourceId },
            }
        );
    }
})
